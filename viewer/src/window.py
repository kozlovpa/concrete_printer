import vtk
from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QMainWindow, QWidget, QLabel, QLineEdit, QComboBox, QGridLayout, QSlider,
                             QCheckBox, QVBoxLayout,
                             QPushButton, QFileDialog, QScrollArea, QGroupBox, QAction, QDialog, QListWidget)
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

from src import locales, gui_utils, interactor_style
from src.gui_utils import plane_tf
from src.settings import sett, get_color

NothingState = "nothing"
GCodeState = "gcode"
StlState = "stl"
BothState = "both"
MovingState = "moving"


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle('Spycer')
        # self.statusBar().showMessage('Ready')

        self.locale = locales.getLocale()

        # Menu
        bar = self.menuBar()
        file_menu = bar.addMenu('File')
        self.open_action = QAction('Open', self)
        # close_action = QAction('Close', self)
        file_menu.addAction(self.open_action)
        # file_menu.addAction(close_action)
        settings_menu = bar.addMenu('Settings')
        self.save_sett_action = QAction('Save', self)
        settings_menu.addAction(self.save_sett_action)

        # main parts
        central_widget = QWidget()
        main_grid = QGridLayout()
        main_grid.addWidget(self.init3d_widget(), 0, 0, 20, 5)
        main_grid.addWidget(self.init_right_panel(), 0, 5, 20, 2)
        central_widget.setLayout(main_grid)
        self.setCentralWidget(central_widget)

        self.state_nothing()

        # ###################TODO:
        self.actors = []
        self.stlActor = None
        # self.colorizeModel()

        # close_action.triggered.connect(self.close)

        ####################

    def init3d_widget(self):
        widget3d = QVTKRenderWindowInteractor()

        self.render = vtk.vtkRenderer()
        self.render.SetBackground(get_color(sett().colors.background))

        widget3d.GetRenderWindow().AddRenderer(self.render)
        self.interactor = widget3d.GetRenderWindow().GetInteractor()
        self.interactor.GetInteractorStyle().SetCurrentStyleToTrackballCamera()

        self.axesWidget = gui_utils.createAxes(self.interactor)

        self.planeActor = gui_utils.createPlaneActorCircle()
        self.planeTransform = vtk.vtkTransform()
        self.render.AddActor(self.planeActor)
        self.boxActors = gui_utils.createBoxActors()
        for b in self.boxActors:
            self.render.AddActor(b)

        self.add_legend()

        self.splanes_actors = []

        self.render.ResetCamera()
        self.render.SetUseDepthPeeling(True)

        widget3d.Initialize()
        widget3d.Start()

        return widget3d

    def add_legend(self):
        hackData =vtk.vtkPolyData() # it is hack to pass value to legend
        hackData.SetPoints(vtk.vtkPoints())

        self.legend = vtk.vtkLegendBoxActor()
        self.legend.SetNumberOfEntries(3)
        self.legend.GetEntryTextProperty().SetFontSize(15)
        self.legend.GetPositionCoordinate().SetCoordinateSystemToNormalizedViewport()
        self.legend.GetPositionCoordinate().SetValue(0, 0)
        self.legend.GetPosition2Coordinate().SetCoordinateSystemToDisplay()
        self.legend.GetPosition2Coordinate().SetValue(290, 3 * 30)
        self.legend.SetEntry(0, hackData, "Rotate - left button.", [1, 1, 1])
        self.legend.SetEntry(1, hackData, "Move - middle button (or shift+left)", [1, 1, 1])
        self.legend.SetEntry(2, hackData, "Scale - right button.", [1, 1, 1])
        self.render.AddActor(self.legend)

    def init_right_panel(self):
        right_panel = QGridLayout()
        right_panel.setSpacing(5)
        # right_panel.setColumnStretch(0, 2)

        # Front-end development at its best
        self.cur_row = 1

        def get_next_row():
            self.cur_row += 1
            return self.cur_row

        def get_cur_row():
            return self.cur_row

        layer_height_label = QLabel(self.locale.LayerHeight)
        self.layer_height_value = QLineEdit(str(sett().slicing.layer_height))
        right_panel.addWidget(layer_height_label, get_next_row(), 1)
        right_panel.addWidget(self.layer_height_value, get_cur_row(), 2)

        extruder_size_label = QLabel(self.locale.ExtruderSize)
        self.extruder_size_value = QLineEdit(str(sett().slicing.extruder_size))
        right_panel.addWidget(extruder_size_label, get_next_row(), 1)
        right_panel.addWidget(self.extruder_size_value, get_cur_row(), 2)

        min_time_per_layer_label = QLabel(self.locale.MinTimePerLayer)
        self.min_time_per_layer_value = QLineEdit(str(sett().slicing.min_time_per_layer))
        right_panel.addWidget(min_time_per_layer_label, get_next_row(), 1)
        right_panel.addWidget(self.min_time_per_layer_value, get_cur_row(), 2)

        max_time_per_layer_label = QLabel(self.locale.MaxTimePerLayer)
        self.max_time_per_layer_value = QLineEdit(str(sett().slicing.max_time_per_layer))
        right_panel.addWidget(max_time_per_layer_label, get_next_row(), 1)
        right_panel.addWidget(self.max_time_per_layer_value, get_cur_row(), 2)

        # BUTTONS
        buttons_layout = QGridLayout()
        buttons_layout.setSpacing(5)
        # buttons_layout.setColumnStretch(0, 2)

        self.slider_label = QLabel(self.locale.LayersCount)
        self.layers_number_label = QLabel()
        buttons_layout.addWidget(self.slider_label, get_next_row(), 1)
        buttons_layout.addWidget(self.layers_number_label, get_cur_row(), 2)

        self.picture_slider = QSlider()
        self.picture_slider.setOrientation(QtCore.Qt.Horizontal)
        self.picture_slider.setMinimum(0)
        self.picture_slider.setValue(0)
        buttons_layout.addWidget(self.picture_slider, get_next_row(), 1, 1, 2)

        self.load_model_button = QPushButton(self.locale.OpenModel)
        buttons_layout.addWidget(self.load_model_button, get_next_row(), 1, 1, 1)

        self.slice3a_button = QPushButton(self.locale.Slice3Axes)
        buttons_layout.addWidget(self.slice3a_button, get_next_row(), 1, 1, 1)

        self.save_gcode_button = QPushButton(self.locale.SaveGCode)
        buttons_layout.addWidget(self.save_gcode_button, get_next_row(), 1, 1, 1)

        panel_widget = QWidget()
        panel_widget.setLayout(right_panel)

        scroll = QScrollArea()
        scroll.setWidget(panel_widget)
        scroll.setWidgetResizable(True)
        # scroll.setFixedHeight(400)

        v_layout = QVBoxLayout()
        v_layout.addWidget(scroll)
        settings_group = QGroupBox('Настройки печати')  # TODO: locale
        settings_group.setLayout(v_layout)

        buttons_group = QWidget()
        buttons_group.setLayout(buttons_layout)

        high_layout = QVBoxLayout()
        high_layout.addWidget(settings_group)
        high_layout.addWidget(buttons_group)
        high_widget = QWidget()
        high_widget.setLayout(high_layout)

        return high_widget

    def switch_stl_gcode(self):
        if self.model_switch_box.isChecked():
            for actor in self.actors:
                actor.VisibilityOff()
            self.stlActor.VisibilityOn()
        else:
            for layer in range(self.picture_slider.value()):
                self.actors[layer].VisibilityOn()
            self.stlActor.VisibilityOff()
        self.reload_scene()

    def clear_scene(self):
        self.render.RemoveAllViewProps()
        self.render.AddActor(self.planeActor)
        self.render.AddActor(self.legend)
        self.rotate_plane(vtk.vtkTransform())
        for b in self.boxActors:
            self.render.AddActor(b)
        for s in self.splanes_actors:
            self.render.AddActor(s)

    def reload_scene(self):
        self.render.Modified()
        self.interactor.Render()

    def change_layer_view(self, prev_value, gcd):

        new_slider_value = self.picture_slider.value()
        if prev_value is None:
            return new_slider_value

        self.actors[new_slider_value - 1].GetProperty().SetColor(get_color(sett().colors.last_layer))
        self.actors[new_slider_value - 1].GetProperty().SetLineWidth(4)
        if len(self.actors) > prev_value - 1:
            self.actors[prev_value - 1].GetProperty().SetColor(get_color(sett().colors.layer))
            self.actors[prev_value - 1].GetProperty().SetLineWidth(1)

        self.layers_number_label.setText(str(new_slider_value))

        if new_slider_value < prev_value:
            for layer in range(new_slider_value, prev_value):
                self.actors[layer].VisibilityOff()
        else:
            for layer in range(prev_value, new_slider_value):
                self.actors[layer].VisibilityOn()

        if gcd.lays2rots[new_slider_value - 1] != gcd.lays2rots[prev_value - 1]:
            curr_rotation = gcd.rotations[gcd.lays2rots[new_slider_value - 1]]
            for block in range(new_slider_value):
                # revert prev rotation firstly and then apply current
                tf = gui_utils.prepareTransform(gcd.rotations[gcd.lays2rots[block]], curr_rotation)
                self.actors[block].SetUserTransform(tf)

            self.rotate_plane(plane_tf(curr_rotation))
            # for i in range(len(self.planes)):
            #     self.rotateAnyPlane(self.planesActors[i], self.planes[i], currRotation)
        self.reload_scene()
        return new_slider_value

    def move_stl2(self):
        if self.move_button.isChecked():
            self.state_moving()

            # self.interactor.SetInteractorStyle(self.actor_interactor_style)

            self.axesWidget.SetEnabled(False)
            if self.boxWidget is None:
                self.boxWidget = vtk.vtkBoxWidget()
                self.boxWidget.SetInteractor(self.interactor)
                # self.boxWidget.SetProp3D(self.stlActor)
                self.boxWidget.SetPlaceFactor(1.25)
                self.boxWidget.SetHandleSize(0.005)
                self.boxWidget.SetEnabled(True)
                self.boxWidget.SetScalingEnabled(False)

                # hack for boxWidget - 1. reset actor transform
                # 2. place boxWidget
                # 3. apply transform to actor and boxWidget
                tf = self.stlActor.GetUserTransform()
                self.stlActor.SetUserTransform(vtk.vtkTransform())
                self.boxWidget.SetProp3D(self.stlActor)
                self.boxWidget.PlaceWidget()
                self.boxWidget.SetTransform(tf)
                self.stlActor.SetUserTransform(tf)

                def TransformActor(obj, event):
                    tf = vtk.vtkTransform()
                    obj.GetTransform(tf)
                    # print(tf.GetScale())
                    self.stlActor.SetUserTransform(tf)
                    self.updateTransform()

                self.boxWidget.AddObserver("InteractionEvent", TransformActor)
            else:
                self.boxWidget.SetEnabled(True)
            # self.interactor.GetInteractorStyle().SetCurrentStyleToTrackballActor()
        else:
            self.state_stl()  # TODO: might be not stl but both or gcode
            # self.interactor.SetInteractorStyle(self.camera_interactor_style)
            self.boxWidget.SetEnabled(False)
            self.axesWidget.SetEnabled(True)
            xc, yc, zmin = gui_utils.findStlOrigin(self.stlActor)
            tf = self.stlActor.GetUserTransform()
            tf.PostMultiply()
            tf.Translate(0, 0, -zmin)
            self.stlActor.SetUserTransform(tf)
            self.boxWidget.SetTransform(tf)
            self.updateTransform()
            # self.interactor.GetInteractorStyle().SetCurrentStyleToTrackballCamera()
        self.reload_scene()

    def updateTransform(self):
        tf = self.stlActor.GetUserTransform()
        x, y, z = tf.GetPosition()
        # self.x_position_value.setText(str(x)[:10])
        # self.y_position_value.setText(str(y)[:10])
        # self.z_position_value.setText(str(z)[:10])
        self.xyz_position_value.setText("Position: " + strF(x) + " " + strF(y) + " " + strF(z))
        a, b, c = tf.GetScale()
        self.xyz_scale_value.setText("Scale: " + strF(a) + " " + strF(b) + " " + strF(c))
        i, j, k = tf.GetOrientation()
        self.xyz_orient_value.setText("Orientation: " + strF(i) + " " + strF(j) + " " + strF(k))

    def open_dialog(self):
        return QFileDialog.getOpenFileName(None, self.locale.OpenModel, "/home/l1va/Downloads/5axes_3d_printer/test",
                                           "STL (*.stl *.STL);;Gcode (*.gcode)")[0]  # TODO: fix path

    def load_stl(self, stl_actor):
        self.clear_scene()
        self.boxWidget = None
        self.stlActor = stl_actor
        # self.actor_interactor_style.setStlActor(self.stlActor)
        self.updateTransform()

        self.render.AddActor(self.stlActor)
        self.state_stl()
        self.render.ResetCamera()
        self.reload_scene()

    def hide_splanes(self):
        if self.hide_checkbox.isChecked():
            for s in self.splanes_actors:
                s.VisibilityOff()
        else:
            for s in self.splanes_actors:
                s.VisibilityOn()
        self.reload_scene()

    def reload_splanes(self, splanes):
        self._recreate_splanes(splanes)
        self.splanes_list.clear()
        for i in range(len(splanes)):
            self.splanes_list.addItem(self.locale.Plane + " " + str(i + 1))

        if len(splanes) > 0:
            self.splanes_list.setCurrentRow(len(splanes) - 1)
        self.reload_scene()

    def _recreate_splanes(self, splanes):
        for p in self.splanes_actors:
            self.render.RemoveActor(p)
        self.splanes_actors = []
        for p in splanes:
            act = gui_utils.create_splane_actor([p.x, p.y, p.z], -60 if p.tilted else 0, p.rot)
            self.splanes_actors.append(act)
            self.render.AddActor(act)

    def update_splane(self, sp, ind):
        self.render.RemoveActor(self.splanes_actors[ind])
        act = gui_utils.create_splane_actor([sp.x, sp.y, sp.z], -60 if sp.tilted else 0, sp.rot)
        self.splanes_actors[ind] = act
        self.render.AddActor(act)
        sel = self.splanes_list.currentRow()
        if sel == ind:
            self.splanes_actors[sel].GetProperty().SetColor(get_color(sett().colors.last_layer))
        self.reload_scene()

    def change_combo_select(self, plane, ind):
        self.tilted_checkbox.setChecked(plane.tilted)
        self.rotated_value.setText(str(plane.rot))
        self.x_value.setText(str(plane.x))
        self.y_value.setText(str(plane.y))
        self.z_value.setText(str(plane.z))
        self.xSlider.setValue(plane.x)
        self.ySlider.setValue(plane.y)
        self.zSlider.setValue(plane.z)
        self.rotSlider.setValue(plane.rot)
        for p in self.splanes_actors:
            p.GetProperty().SetColor(get_color(sett().colors.splane))
        self.splanes_actors[ind].GetProperty().SetColor(get_color(sett().colors.last_layer))
        self.reload_scene()

    def load_gcode(self, actors, is_from_stl, plane_tf):
        self.clear_scene()
        if is_from_stl:
            self.stlActor.VisibilityOff()
            self.render.AddActor(self.stlActor)

        self.rotate_plane(plane_tf)

        self.actors = actors
        for actor in self.actors:
            self.render.AddActor(actor)

        if is_from_stl:
            self.state_both(len(self.actors))
        else:
            self.state_gcode(len(self.actors))

        self.render.ResetCamera()
        self.reload_scene()

    def rotate_plane(self, tf):
        self.planeActor.SetUserTransform(tf)
        self.planeTransform = tf

    def save_gcode_dialog(self):
        return QFileDialog.getSaveFileName(None, self.locale.SaveGCode, "", "Gcode (*.gcode)")[0]

    def state_nothing(self):
        # self.model_switch_box.setEnabled(False)
        # self.model_switch_box.setChecked(False)
        self.slider_label.setEnabled(False)
        self.layers_number_label.setEnabled(False)
        self.layers_number_label.setText(" ")
        self.picture_slider.setEnabled(False)
        self.picture_slider.setSliderPosition(0)
        # self.smoothSlice_button.setEnabled(False)
        # self.move_button.setEnabled(False)
        self.load_model_button.setEnabled(True)
        self.slice3a_button.setEnabled(False)
        # self.color_model_button.setEnabled(False)
        # self.analyze_model_button.setEnabled(False)
        # self.edit_planes_button.setEnabled(False)
        # self.slice_vip_button.setEnabled(False)
        self.save_gcode_button.setEnabled(False)
        # self.bottom_panel.setEnabled(False)
        self.state = NothingState

    def state_gcode(self, layers_count):
        self.model_switch_box.setEnabled(False)
        self.model_switch_box.setChecked(False)
        self.slider_label.setEnabled(True)
        self.layers_number_label.setEnabled(True)
        self.layers_number_label.setText(str(layers_count))
        self.picture_slider.setEnabled(True)
        self.picture_slider.setMaximum(layers_count)
        self.picture_slider.setSliderPosition(layers_count)
        self.smoothSlice_button.setEnabled(False)
        self.move_button.setEnabled(False)
        self.load_model_button.setEnabled(True)
        self.slice3a_button.setEnabled(False)
        # self.color_model_button.setEnabled(False)
        # self.analyze_model_button.setEnabled(False)
        self.edit_planes_button.setEnabled(True)
        # self.slice_vip_button.setEnabled(False)
        self.save_gcode_button.setEnabled(True)
        self.bottom_panel.setEnabled(False)
        self.state = GCodeState

    def state_stl(self):
        self.model_switch_box.setEnabled(False)
        self.model_switch_box.setChecked(True)
        self.slider_label.setEnabled(False)
        self.layers_number_label.setEnabled(False)
        self.layers_number_label.setText(" ")
        self.picture_slider.setEnabled(False)
        self.picture_slider.setSliderPosition(0)
        self.smoothSlice_button.setEnabled(True)
        self.move_button.setEnabled(True)
        self.load_model_button.setEnabled(True)
        self.slice3a_button.setEnabled(True)
        self.color_model_button.setEnabled(True)
        self.analyze_model_button.setEnabled(False)
        self.edit_planes_button.setEnabled(False)
        self.slice_vip_button.setEnabled(True)
        self.save_gcode_button.setEnabled(False)
        self.bottom_panel.setEnabled(True)
        self.state = StlState

    def state_moving(self):
        self.model_switch_box.setEnabled(False)
        self.model_switch_box.setChecked(True)
        self.slider_label.setEnabled(False)
        self.layers_number_label.setEnabled(False)
        self.layers_number_label.setText(" ")
        self.picture_slider.setEnabled(False)
        self.picture_slider.setSliderPosition(0)
        self.smoothSlice_button.setEnabled(False)
        self.move_button.setEnabled(True)
        self.load_model_button.setEnabled(False)
        self.slice3a_button.setEnabled(False)
        self.color_model_button.setEnabled(False)
        self.analyze_model_button.setEnabled(False)
        self.edit_planes_button.setEnabled(False)
        self.slice_vip_button.setEnabled(False)
        self.save_gcode_button.setEnabled(False)
        self.bottom_panel.setEnabled(False)
        self.state = MovingState

    def state_both(self, layers_count):
        self.model_switch_box.setEnabled(True)
        self.model_switch_box.setChecked(False)
        self.slider_label.setEnabled(True)
        self.layers_number_label.setEnabled(True)
        self.layers_number_label.setText(str(layers_count))
        self.picture_slider.setEnabled(True)
        self.picture_slider.setMaximum(layers_count)
        self.picture_slider.setSliderPosition(layers_count)
        self.smoothSlice_button.setEnabled(True)
        self.move_button.setEnabled(True)
        self.load_model_button.setEnabled(True)
        self.slice3a_button.setEnabled(True)
        self.color_model_button.setEnabled(True)
        self.analyze_model_button.setEnabled(False)
        self.edit_planes_button.setEnabled(True)
        self.slice_vip_button.setEnabled(True)
        self.save_gcode_button.setEnabled(True)
        self.bottom_panel.setEnabled(True)
        self.state = BothState


def strF(v):  # cut 3 numbers after the point in float
    s = str(v)
    i = s.find(".")
    if i != -1:
        s = s[:min(len(s), i + 3)]
    return s
